function limitFunctionCallCount(cb,n)
{
    if(typeof cb !=='function' || typeof n !=='number' || n<0 || n===undefined)
    {
        throw new Error("Invalid Attempt");
    }
    let number_executed=0;
    return function closure(...args)
    {
        number_executed++;
        if(number_executed<=n)
        {
            return cb(...args);
        }
        else
        {
            return null;
        }
    };
};
module.exports=limitFunctionCallCount;