function cacheFunction(cb)
{
    if(typeof cb!=='function')
    {
        throw new Error("Invalid parameter: it should be a function");
    }
    const cache={};
    return function(...args)
    {
        const key=JSON.stringify(args);
        if(cache.hasOwnProperty(key))
        {
            return cache[key];
        }
        const result=cb(...args);
        cache[key]=result;
        return result;
    };
};
module.exports=cacheFunction;