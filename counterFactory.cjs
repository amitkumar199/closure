function counterFactory()
{
    let counter =0;
    return {
        increment: function increment(){
            counter++;
            return counter;
        },
        decrement: function decrement(){
            counter--;
            return counter;
        }
    };
};
module.exports=counterFactory;